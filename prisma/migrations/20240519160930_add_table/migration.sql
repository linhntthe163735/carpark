/*
  Warnings:

  - The primary key for the `Employee` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `email` on the `Employee` table. All the data in the column will be lost.
  - You are about to drop the column `id` on the `Employee` table. All the data in the column will be lost.
  - You are about to drop the column `name` on the `Employee` table. All the data in the column will be lost.
  - Added the required column `account` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `department` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeeAddress` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeeBirthdate` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeeEmail` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeeId` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeeName` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `employeePhone` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `password` to the `Employee` table without a default value. This is not possible if the table is not empty.
  - Added the required column `sex` to the `Employee` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX `Employee_email_key` ON `Employee`;

-- AlterTable
ALTER TABLE `Employee` DROP PRIMARY KEY,
    DROP COLUMN `email`,
    DROP COLUMN `id`,
    DROP COLUMN `name`,
    ADD COLUMN `account` VARCHAR(50) NOT NULL,
    ADD COLUMN `department` VARCHAR(10) NOT NULL,
    ADD COLUMN `employeeAddress` VARCHAR(50) NOT NULL,
    ADD COLUMN `employeeBirthdate` DATE NOT NULL,
    ADD COLUMN `employeeEmail` VARCHAR(50) NOT NULL,
    ADD COLUMN `employeeId` INTEGER NOT NULL AUTO_INCREMENT,
    ADD COLUMN `employeeName` VARCHAR(50) NOT NULL,
    ADD COLUMN `employeePhone` VARCHAR(10) NOT NULL,
    ADD COLUMN `password` VARCHAR(20) NOT NULL,
    ADD COLUMN `sex` VARCHAR(1) NOT NULL,
    ADD PRIMARY KEY (`employeeId`);

-- CreateTable
CREATE TABLE `Trip` (
    `tripId` INTEGER NOT NULL AUTO_INCREMENT,
    `bookedTicketNumber` INTEGER NOT NULL,
    `carType` VARCHAR(11) NOT NULL,
    `departureDate` DATE NOT NULL,
    `departureTime` VARCHAR(191) NOT NULL,
    `destination` VARCHAR(50) NOT NULL,
    `driver` VARCHAR(11) NOT NULL,
    `maximumOnlineTicketNumber` INTEGER NOT NULL,

    PRIMARY KEY (`tripId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `BookingOffice` (
    `officeId` INTEGER NOT NULL AUTO_INCREMENT,
    `endContractDeadline` DATE NOT NULL,
    `officeName` VARCHAR(50) NOT NULL,
    `officePhone` VARCHAR(11) NOT NULL,
    `officePlace` VARCHAR(50) NOT NULL,
    `officePrice` INTEGER NOT NULL,
    `startContractDeadline` DATE NOT NULL,
    `tripId` INTEGER NOT NULL,

    UNIQUE INDEX `BookingOffice_tripId_key`(`tripId`),
    PRIMARY KEY (`officeId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ParkingIot` (
    `parkId` INTEGER NOT NULL AUTO_INCREMENT,
    `parkArea` INTEGER NOT NULL,
    `parkName` VARCHAR(191) NOT NULL,
    `partPlace` VARCHAR(191) NOT NULL,
    `parkPrice` INTEGER NOT NULL,
    `parkStatus` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`parkId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Car` (
    `licensePlate` VARCHAR(191) NOT NULL,
    `carColor` VARCHAR(11) NOT NULL,
    `carType` VARCHAR(50) NOT NULL,
    `company` VARCHAR(50) NOT NULL,
    `parkId` INTEGER NOT NULL,

    UNIQUE INDEX `Car_parkId_key`(`parkId`),
    PRIMARY KEY (`licensePlate`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Ticket` (
    `ticketId` INTEGER NOT NULL AUTO_INCREMENT,
    `bookingTime` TIME NOT NULL,
    `customerName` VARCHAR(11) NOT NULL,
    `tripId` INTEGER NOT NULL,
    `licensePlate` VARCHAR(191) NOT NULL,

    UNIQUE INDEX `Ticket_tripId_key`(`tripId`),
    UNIQUE INDEX `Ticket_licensePlate_key`(`licensePlate`),
    PRIMARY KEY (`ticketId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `BookingOffice` ADD CONSTRAINT `BookingOffice_tripId_fkey` FOREIGN KEY (`tripId`) REFERENCES `Trip`(`tripId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Car` ADD CONSTRAINT `Car_parkId_fkey` FOREIGN KEY (`parkId`) REFERENCES `ParkingIot`(`parkId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Ticket` ADD CONSTRAINT `Ticket_tripId_fkey` FOREIGN KEY (`tripId`) REFERENCES `Trip`(`tripId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Ticket` ADD CONSTRAINT `Ticket_licensePlate_fkey` FOREIGN KEY (`licensePlate`) REFERENCES `Car`(`licensePlate`) ON DELETE RESTRICT ON UPDATE CASCADE;
