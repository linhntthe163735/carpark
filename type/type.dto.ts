export type EmployeeType = {
    id?: number; // Optional because it's auto-generated
    account: string;
    department: string;
    address: string;
    birthdate: Date | string; // Allow string for incoming JSON, but convert to Date
    email: string;
    name: string;
    phone: string;
    password: string;
    sex: string;
};


export type TripType = {

    Id: number;
    bookedTicketNumber: number;
    carType: string;
    departureDate: Date;
    departureTime: string;
    destination: string;
    driver: string;
    maximumOnlineTicketNumber: number

};


export type BookingOffice ={
    id: number;
    endContractDeadline: Date | string;
    officeName: string;
    officePhone:string;
    officePlace: string;
    officePrice: number;
    startContractDeadline: Date | string;
    tripId: number;

}

export type CarType = {
    licensePlate?: string; // Optional because it's auto-generated
    carColor: string;
    carType: string;
    company: string;
    parkingLotId?: number; // Include parkingLotId property
};



export type ParkingLotType = {
    parkArea: number;
    parkName: string;
    partPlace: string;
    parkPrice: number;
    parkStatus: string;
};



