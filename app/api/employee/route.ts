import { NextResponse } from 'next/server';
import prisma from '@/prisma'; // Ensure this path is correct based on your project structure
import { EmployeeType } from '@/type/type.dto';
import { Prisma } from '@prisma/client';

export const GET = async () => {
    try {
        const employees = await prisma.employee.findMany();
        return NextResponse.json({ message: "Get all employees successful", employees }, { status: 200 });
    } catch (err) {
        return NextResponse.json({ message: "Error getting all employees", err }, { status: 500 });
    }
}

export const POST = async (req: Request) => {
    try {
        const employeeRequest: EmployeeType = await req.json();

        // Convert birthdate to a Date object
        employeeRequest.birthdate = new Date(employeeRequest.birthdate);

        // Create the employee in the database
        const employee = await prisma.employee.create({
            data: {
                account: employeeRequest.account,
                department: employeeRequest.department,
                address: employeeRequest.address,
                birthdate: employeeRequest.birthdate,
                email: employeeRequest.email,
                name: employeeRequest.name,
                phone: employeeRequest.phone,
                password: employeeRequest.password,
                sex: employeeRequest.sex,
            },
        });

        return NextResponse.json({ message: "Create an employee successful", employee }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error creating employee", err }, { status: 500 });
    }
}


