import { NextResponse } from "next/server";
import prisma from "@/prisma"
import { EmployeeType } from "@/type/type.dto";



export async function GET(req: Request, { params }:{params: { id: string }}){
  try {
    const { id } = params;

    if (!id) {
      return new NextResponse('Employee ID is required', { status: 400 });
    }

    const employee = await prisma.employee.findUnique({
      where: {
        id: parseInt(id),
      },
    });

    if (!employee) {
      return NextResponse.json({ message: 'Employee not found!' }, { status: 404 });
    }

    return NextResponse.json(
      { message: 'Get employee by id successful', employee },
      { status: 200 }
    );
  } catch (err) {
    console.log(err);
    return new NextResponse('Error: get employee by id failed', { status: 500 });
  } finally {
    await prisma.$disconnect();
  }
}

export async function DELETE(req: Request, { params }:{params: { id: string }}) {
    try {
      const { id } = params;
  
      if (!id) {
        return new NextResponse('Employee ID is required', { status: 400 });
      }
  
      const deletedEmployee = await prisma.employee.delete({
        where: {
          id: parseInt(id),
        },
      });
  
      return NextResponse.json(
        { message: 'Employee deleted successfully', deletedEmployee },
        { status: 200 }
      );
    } catch (err) {
      console.log(err);
   
      return new NextResponse('Error: delete employee failed', { status: 500 });
    } finally {
      await prisma.$disconnect();
    }
  }

  export async function PUT(req: Request, { params }:{params: { id: string }}) {
    try {
      const { id } = params;
  
      if (!id) {
        return new NextResponse('Employee ID is required', { status: 400 });
      }
  
      const body = await req.json();
      const { account, department, address, birthdate, email, name, phone, password, sex } = body;
  
      const updatedEmployee = await prisma.employee.update({
        where: { id: parseInt(id) },
        data: {
          account,
          department,
          address,
          birthdate: new Date(birthdate),
          email,
          name,
          phone,
          password,
          sex,
        },
      });
  
      return NextResponse.json(
        { message: 'Employee updated successfully', updatedEmployee },
        { status: 200 }
      );
    } catch (err) {
      console.log(err);
      
      return new NextResponse('Error: update employee failed', { status: 500 });
    } finally {
      await prisma.$disconnect();
    }
  }