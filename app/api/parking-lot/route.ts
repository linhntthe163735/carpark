import { NextResponse } from 'next/server';
import prisma from '@/prisma'; // Adjust the import path based on your project structure
import { ParkingIot } from '@prisma/client';

export const GET = async () => {
    try {
        const parkingLots = await prisma.parkingIot.findMany();
        return NextResponse.json({ message: "Get all parking lots successful", parkingLots }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error getting parking lots", err: err.message }, { status: 500 });
    }
};


export const POST = async (req: Request) => {
    try {
        const parkingLotRequest: ParkingIot = await req.json();
        
        // Implement validation for the input data here

        const parkingLot = await prisma.parkingIot.create({
            data: parkingLotRequest,
        });

        return NextResponse.json({ message: "Add parking lot successful", parkingLot }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error adding parking lot", err: err.message }, { status: 500 });
    }
};

