import { NextResponse } from 'next/server';
import prisma from '@/prisma'; // Adjust the import path based on your project structure
import { TripType } from '@/type/type.dto';

export const GET = async () => {
    try {
        const trips = await prisma.trip.findMany();
        return NextResponse.json({ message: "Get all trips successful", trips }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error getting trips", err: err.message }, { status: 500 });
    }
};

export const POST = async (req: Request) => {
    try {
        const tripRequest: TripType = await req.json();
        
        // Implement validation for the input data here

        const trip = await prisma.trip.create({
            data: tripRequest,
        });

        return NextResponse.json({ message: "Add trip successful", trip }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error adding trip", err: err.message }, { status: 500 });
    }
};







