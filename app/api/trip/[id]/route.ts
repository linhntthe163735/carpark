import { NextResponse } from "next/server";
import prisma from "@/prisma"
import { TripType } from "@/type/type.dto";

export async function GET(req: Request, { params }:{params: { id: string }}){
  try {
    const { id } = params;

    if (!id) {
      return new NextResponse('Trip ID is required', { status: 400 });
    }

    const trip = await prisma.trip.findUnique({
      where: {
        id: parseInt(id),
      },
    });

    if (!trip) {
      return NextResponse.json({ message: 'Trip not found!' }, { status: 404 });
    }

    return NextResponse.json(
      { message: 'Get trip by id successful', trip },
      { status: 200 }
    );
  } catch (err) {
    console.log(err);
    return new NextResponse('Error: get trip by id failed', { status: 500 });
  } finally {
    await prisma.$disconnect();
  }
}

export async function DELETE(req: Request, { params }:{params: { id: string }}) {
    try {
      const { id } = params;
  
      if (!id) {
        return new NextResponse('Trip ID is required', { status: 400 });
      }
  
      const deletedTrip = await prisma.trip.delete({
        where: {
          id: parseInt(id),
        },
      });
  
      return NextResponse.json(
        { message: 'Trip deleted successfully', deletedTrip },
        { status: 200 }
      );
    } catch (err) {
      console.log(err);
      return new NextResponse('Error: delete trip failed', { status: 500 });
    } finally {
      await prisma.$disconnect();
    }
  }

  export async function PUT(req: Request, { params }:{params: { id: string }}) {
    try {
      const { id } = params;
  
      if (!id) {
        return new NextResponse('Trip ID is required', { status: 400 });
      }
  
      const body = await req.json();
      const { bookedTicketNumber, carType, departureDate, departureTime, destination, driver, maximumOnlineTicketNumber } = body;
  
      const updatedTrip = await prisma.trip.update({
        where: { id: parseInt(id) },
        data: {
          bookedTicketNumber,
          carType,
          departureDate: new Date(departureDate),
          departureTime,
          destination,
          driver,
          maximumOnlineTicketNumber,
        },
      });
  
      return NextResponse.json(
        { message: 'Trip updated successfully', updatedTrip },
        { status: 200 }
      );
    } catch (err) {
      console.log(err);
      return new NextResponse('Error: update trip failed', { status: 500 });
    } finally {
      await prisma.$disconnect();
    }
  }
