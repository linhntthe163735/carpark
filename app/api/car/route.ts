import { NextResponse } from 'next/server';
import prisma from '@/prisma'; // Adjust the import path based on your project structure
import { CarType } from '@/type/type.dto';

export const GET = async () => {
    try {
        const cars = await prisma.car.findMany();
        return NextResponse.json({ message: "Get all cars successful", cars }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error getting cars", err }, { status: 500 });
    }
};

export const POST = async (req: Request) => {
    try {
        const carRequest: CarType = await req.json();
        
        // Implement validation for the input data here

        const car = await prisma.car.create({
            data: {
                carColor: carRequest.carColor,
                carType: carRequest.carType,
                company: carRequest.company,
                parkingLot: {
                    connect: carRequest.parkingLotId ? { id: carRequest.parkingLotId } : undefined
                }
            },
        });

        return NextResponse.json({ message: "Add car successful", car }, { status: 200 });
    } catch (err) {
        console.error(err);
        return NextResponse.json({ message: "Error adding car", err }, { status: 500 });
    }
};
