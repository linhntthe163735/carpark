import { NextResponse } from 'next/server';
import prisma from '@/prisma'; // Adjust the import path based on your project structure
import { BookingOffice } from '@prisma/client';

export const GET = async () => {
    try {
        const bookingOffices = await prisma.bookingOffice.findMany();
        return NextResponse.json({ message: "Get all booking offices successful", bookingOffices }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error getting booking offices", err}, { status: 500 });
    }
};

export const POST = async (req: Request) => {
    try {
        const bookingOfficeRequest: BookingOffice = await req.json();
        
        // Perform validation on bookingOfficeRequest if needed

        const bookingOffice = await prisma.bookingOffice.create({
            data: bookingOfficeRequest,
        });

        return NextResponse.json({ message: "Add booking office successful", bookingOffice }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error adding booking office", err }, { status: 500 });
    }
};


